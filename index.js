console.log("hello");

// NAKUHA KO DIN!!! #3&4
async function fetchData(){

		let result = await(fetch("https://jsonplaceholder.typicode.com/todos"));
	//console.log(result);
	
	let json = await result.json();
	// console.log(json);

	let title = await json.map(({title}) => ({title}));
	console.log(title);
}
fetchData();

// OR

// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((json) => {

// 	let list = json.map((todo => {
// 		return todo.title;
// 	}))

// 	console.log(list);

// });







//#5-6 GET
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
// .then((response) => console.log(response))
.then((json) => (console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)));

//#7 POST 
fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Created To Do List Item",
			completed: false,
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

// #8 PUT

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

// #10 PATCH

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete"
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));


// #12 DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "DELETE"
	}
)
.then(response => response.json())
.then(response => console.log(response));

